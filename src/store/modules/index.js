const modulesInDirectory = require.context('.', false, /\.store\.js$/);
const modules = {};

// It's very important  to name file's store like module.store.js for the autoload */
modulesInDirectory.keys().forEach(module  => {

  const moduleName = module
    .replace(/(\.\/|\.store\.js)/g, '')
    .replace(/^\w/, c => c.toUpperCase());

  modules[moduleName] = modulesInDirectory(module).default || modulesInDirectory(module);
});
export default modules;

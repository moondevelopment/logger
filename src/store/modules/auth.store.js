import AuthService from "@/services/auth.service"
import storage from '@/utils/storage'


const user = storage.getUser();
const initialState = user ? { loggedIn: true , user } : { loggedIn: false , user: null };

export default {
  namespaced: true,
  state: initialState,
  actions: {
    async login({commit}, user) {
      try {
        const res = await AuthService.login(user);
        commit("LOGIN_SUCCESS", res);
        return res;
      }
      catch (e) {
        commit("LOGIN_FAILURE");
        throw e;
      }
    },
    async logout({commit}) {
      try {
        const res = await AuthService.logout();
        commit("LOGOUT_SUCCESS", res);
        return res;
      }
      catch (e) {
        throw e;
      }
    }
  },
  mutations: {
    LOGIN_SUCCESS (state, user) {
      state.loggedIn = true;
      state.user = user;
    },
    LOGIN_FAILURE(state) {
      state.loggedIn = true;
      state.user = null;
    },
    LOGOUT_SUCCESS(state) {
      state.loggedIn = false;
      state.user = null;
    }
  }
}

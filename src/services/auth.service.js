import http from '@/utils/http'
import storage from '@/utils/storage'
import AppErrors from "@/consts/application-errors"

export default {

  async login({username, password}) {
    try {
      //const res = await http.post("user-login", {username, password});
      if(username === "saliou" && password === "hey") {
        const res = {data : { token: "hjdshjkfhsdfkjdshfkjsdhfsdf", name: "SARR", firstname : "Saliou"}};
        const user = res.data;
        if(user.token) storage.setUser(JSON.stringify(user));
        return user;
      }
      else {
        throw AppErrors.INVALID_CREDIT;
      }
    }
    catch (error) {
      if(error.response && error.response.status === 401) {
        throw AppErrors.INVALID_CREDIT
      } else {
        throw error;
      }
    }
  },

  logout() {
    localStorage.removeItem('user');
  },

  async register({email, username, password}) {

    try {
      const res = await http.post("user-register", {email, username, password});

      const user = res.data;
      if(user.token) storage.setUser(JSON.stringify(user));
      return user;
    }
    catch (error) {
      throw error;
    }
  }
}

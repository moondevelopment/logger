import storage from '@/utils/storage'
export default function () {
  const user = storage.getUser() && JSON.parse(storage.getUser())
  return (user && user.token) ? { Authorization: 'Bearer ' + user.accessToken} : {};
}

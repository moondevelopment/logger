import http from '@/utils/http'
import authHeader from './auth-header'

const BASE_URL = "ajax-log-front.php";
export default {
  async getLogs() {
    const res = await http.get(`${BASE_URL}?getlog=true`, {header: authHeader()});
    return res.data.map(log => deserializeLog(log))
  },
  async getContacts() {
    const res = await http.get(`${BASE_URL}?getcontacts=true`, {header: authHeader()});
    return res.data
  }
}


function deserializeLog(log) {
  return {
    origin: log.Log_origin,
    reason: log.Log_reason,
    action: log.Log_action,
    data: log.Log_datas,
    date: log.Log_created
  }
}

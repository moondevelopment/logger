export default {
  getUser () {
    return localStorage.getItem('user');
  },
  setUser(user) {
    localStorage.setItem('user', user);
  }
}

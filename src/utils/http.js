import Axios from 'axios'
const prod = "https://www.leguidedupatrimoine.com/controllers/";
const local = "http://localhost/gdp.com/controllers/";
const axios = Axios.create({
  baseURL: prod,
  headers: {
    Accept: 'application/json'
  }
})


export default {
  async get (url, header) {
    console.assert(typeof url === "string");
    return await axios.get(url, header)
  },
  async post(url, data, header = {})  {
    console.assert(typeof url === "string");
    return await axios.post(url, data, header);
  },
  async put(url, data, header  = {})  {
    console.assert(typeof url === "string");
    return await axios.put(url, data, header);
  },
  async delete(url, header  = {})  {
    console.assert(typeof url === "string");
    return await axios.delete(url, header);
  }
}
